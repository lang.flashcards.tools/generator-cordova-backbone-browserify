'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');
var chalk = require('chalk');


var PhonegapBrowserifyBackboneGenerator = yeoman.generators.Base.extend({

  init: function () {
    this.pkg = require('../package.json');

    /*this.on('end', function () {
      if (!this.options['skip-install']) {
        this.installDependencies();
      }
    });*/

    var me = this;
    /*this.on('method', function(method){
      console.log('METHOD:' + method);
    });*/
    this.on('npmInstall:end', function(paths){
      me.log('npm installed with paths');
    });

    this.on('bowerInstall:end', function(paths){
      me.log('bower installed with paths');
    });
  },

  askFor: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay('Welcome to Cordova Backbone Browserify generator!'));

    var parts = this.destinationRoot().split('/');
    var dirName = parts[parts.length - 1];
    var prompts = [
      {
        type : 'input',
        name : 'name',
        message : 'name: application package name ?',
        default : dirName
      },
      {
        type : 'input',
        name : 'namespace',
        message : 'namespace: application package namespace ?',
        default : dirName + '.test.app'
      },
      {
        type : 'input',
        name : 'version',
        message : 'version: ?',
        default : '0.0.0'
      },
      {
        type : 'input',
        name : 'author',
        message : 'author name: ?',
        default : this.user.git.username + ' <' + this.user.git.email + '>'
      },
      {
        type : 'input',
        name : 'androidMinSdkVersion',
        message : 'android-minSdkVersion: ?',
        default : '14'
      },
      {
        type : 'input',
        name : 'androidMaxSdkVersion',
        message : 'android-maxSdkVersion: ?',
        default : '23'
      },
      {
        type : 'input',
        name : 'androidTargetSdkVersion',
        message : 'android-targetSdkVersion: ?',
        default : '22'
      },
      {
        type : 'input',
        name : 'description',
        message : 'description: ?',
        default : 'test application'
      },
      {
        type : 'input',
        name : 'addGitignore',
        message : 'add .gitignore files [yes|no] ?',
        default : 'yes'
      },
    ];

    this.prompt(prompts, function (props) {
      this._inputData = props;
      console.log(props);
      done();
    }.bind(this));
  },

  configuration : function(){
    this.template('_package.json', 'package.json', this._inputData);
    this.template('config.xml', 'config.xml', this._inputData);
    this.template('_Gruntfile.js', 'Gruntfile.js', this._inputData);
  },

  resources: function(){
    this.mkdir('src');
    var rsDir = 'src/resources';
    this.mkdir(rsDir);
    this.template(rsDir + '/index-template.html', rsDir + '/index-template.html', this._inputData);
    this.mkdir(rsDir + '/sass');
    this.template(rsDir + '/sass/index.scss', rsDir + '/sass/index.scss', this._inputData);
    this.copy(rsDir + '/sass/_home-view.scss', rsDir + '/sass/_home-view.scss');
    this.mkdir(rsDir + '/templates');
    this.template(rsDir + '/templates/home-view.html', rsDir + '/templates/home-view.html', this._inputData);

    var jsDir = 'src/js';
    this.mkdir(jsDir);
    this.template(jsDir + '/App.js', jsDir + '/App.js', this._inputData);
    this.template(jsDir + '/main.js', jsDir + '/main.js', this._inputData);
    this.mkdir(jsDir + '/views');
    this.template(jsDir + '/views/HomeView.js', jsDir + '/views/HomeView.js', this._inputData);
    this.copy(jsDir + '/utils.js', jsDir + '/utils.js');

    if(this._inputData.addGitignore === 'yes'){
      this.copy('_gitignore', '.gitignore');
      this.copy(jsDir + '/_gitignore', jsDir + '/.gitignore');
      this.copy(rsDir + '/_gitignore', rsDir + '/.gitignore');
    }
  },


  installServerPackages : function(){
    this.npmInstall([],{});
  },

  _installNpms : function(){
    var me = this;
    this.npmInstall([],{},function(){
      me.log('npm packages installed');
    });
  }
});

module.exports = PhonegapBrowserifyBackboneGenerator;
