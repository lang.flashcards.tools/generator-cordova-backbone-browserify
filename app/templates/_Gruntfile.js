var _ = require("underscore");

module.exports = function(grunt){
    'use strict';

    var browserWwwDir = 'platforms/browser/www/';

    // Project configuration.
    grunt.initConfig({

        cordova : 'node_modules/.bin/cordova',
        pkg: grunt.file.readJSON('package.json'),
        appDir : '<%%= pkg.name %>',
        generatedAppDir : 'temp-app',

        clean: {
            project: [
                'src/resources/css/*.css',
                'src/resources/sass/_app.scss',
                'src/resources/fonts/glyphicons-halflings-regular.ttf',
                'src/<%%= pkg.bundle %>.jst.min.js',
                'src/<%%= pkg.bundle %>.min.js',
                'src/app/version.js'
            ],
            www : ['www/'],
            fonts : ['src/resources/fonts'],
            css : ['src/resources/css'],
            jst : ['src/jst.min.js'],
            index : ['src/index.html'],
            browser:[ 'platforms/browser', 'plugins/browser.json'],
            ios:[ 'platforms/ios', 'plugins/ios.json'],
            android:[ 'platforms/android', 'plugins/android.json'],
            'generated' : [
                '.cordova',
                'hooks',
                'platforms',
                'plugins'
            ],
            'generated-app' : [ '<%%= generatedAppDir %>'],
            'hammer-jquery' : ['node_modules/jquery-hammerjs/node_modules/jquery'] // removie jquery duplicate
        },

        copy: {
            'generated' : {
                files: [
                    {
                        expand: true,
                        cwd: '<%%= generatedAppDir %>',
                        src : [
                            'plugins/**/*',
                            'hooks/**/*',
                            'platforms/**/*'
                        ],
                        dest: '.'
                    }
                ]
            },
            bootstrap: {
                files: [
                    {
                        expand: true,
                        cwd: 'node_modules/bootstrap/dist/css',
                        src: [
                            'bootstrap.css',
                            'bootstrap.css.map',
                        ],
                        dest: 'src/resources/css/'
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/bootstrap/dist/fonts',
                        src: [
                            '*'
                        ],
                        dest: 'src/resources/fonts/'
                    }
                ]
            },
            fontawesome: {
                files: [
                    {
                        expand: true,
                        cwd: 'node_modules/font-awesome/css',
                        src: [
                            'font-awesome.css'
                        ],
                        dest: 'src/resources/css/'
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/font-awesome/fonts',
                        src: [
                            '*'
                        ],
                        dest: 'src/resources/fonts/'
                    }
                ]
            },
            index : {
                files: [
                    {
                        src : 'src/resources/index-template.html',
                        dest: 'src/index.html'
                    }
                ]
            },
            'to-www-index': {
                files: [
                    {
                        src : 'src/index.html',
                        dest: 'www/index.html'
                    }
                ]
            },
            'to-www-js': {
                files: [
                    {
                        expand: true,
                        cwd: 'src/js',
                        src : 'index.js',
                        dest: 'www/js/'
                    }
                ]
            },
            'to-www-templates': {
                files: [
                    {
                        expand: true,
                        cwd: 'src/js',
                        src : 'jst.js',
                        dest: 'www/js/'
                    }
                ]
            },
            'to-www-css': {
                files: [
                    {
                        expand: true,
                        cwd: 'src/resources/',
                        src: ['css/*.css'],
                        dest: 'www/'
                    }
                ]
            },
            'to-www-fonts': {
                files: [
                    {
                        expand: true,
                        cwd: 'src/resources/fonts',
                        src : '**/*',
                        dest: 'www/fonts/'
                    }
                ]
            },
            'www-to-browser-index' : {
                files: [
                    {
                        src : 'www/index.html',
                        dest: browserWwwDir + 'index.html'
                    }
                ]
            },
            'www-to-browser-js' : {
                files: [
                    {
                        expand: true,
                        cwd: 'www/js',
                        src : ['**/*.js'],
                        dest: browserWwwDir + 'js/'
                    }
                ]
            },
            'www-to-browser-templates' : {
                files: [
                    {
                        expand: true,
                        cwd: 'src/', // intended source
                        src : 'jst.min.js',
                        dest: browserWwwDir + 'js/'
                    }
                ]
            },
            'www-to-browser-css' : {
                files : [
                    {
                        expand: true,
                        cwd: 'www/css/',
                        src: ['**/*'],
                        dest: browserWwwDir + 'css/'
                    }
                ]
            },
            'www-to-browser-fonts' : {
                files: [
                    {
                        expand: true,
                        cwd: 'www/fonts',
                        src : ['**/*'],
                        dest: browserWwwDir + 'fonts/'
                    }
                ]
            }
        },

        exec: {
            'create-app' : {
                cwd: '.',
                command: '<%%= cordova %> create <%%= generatedAppDir %> <%%= pkg.bundle %> <%%= appName %>'
            },
            'add-platform-ios' : {
                cwd : '.',
                command : '<%%= cordova %> platform add ios'
            },
            'add-platform-android' : {
                cwd : '.',
                command : '<%%= cordova %> platform add android'
            },
            'add-platform-browser' : { // for testing
                cwd : '.',
                command : '<%%= cordova %> platform add browser'
            },
            'build-ios' : {
                cwd: '.',
                command: '<%%= cordova %> build ios'
            },
            'build-android' : {
                cwd: '.',
                command: '<%%= cordova %> build android'
            },
            'build-browser' : {
                cwd : '.',
                command: '<%%= cordova %> build browser'
            },
            'serve-browser' : {
                cwd : '.',
                command: '<%%= cordova %> run browser'
            },
            'plugin-console' : {
                cwd : '.',
                command : '<%%= cordova %> plugin add cordova-plugin-console@1.0.1'
            }
        },

        jshint: { //TODO: review
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                boss: true,
                eqnull: true,
                node: true,
                globals: {
                    exports: true,
                    zepto: true,
                    $: true,
                    _: true,
                    xit: true,
                    //document
                    window: false,
                    document: false,
                    // require.js
                    define: false,
                    // plug-ins
                    gaPlugin: false,
                    JREngage: false,
                    ExtractZipFile : false,
                    LocalFileSystem : false,
                    google : false,
                    // mocha testing
                    mochaPhantomJS : false
                }
            },
            src: 'src/js/**/*.js'
        },

        browserify : {
            index : {
                files : {
                    'src/js/index.js' : ['src/js/main.js']
                },
                options: {
                    ignore : ['src/js/index.js']
                }
            }
        },

        'browserify-jst' : {
            'templates' : {
                src : 'src/resources/templates/**/*.html',
                dest : 'src/js/jst.js',
                options : {
                    processName : function(filepath){
                        var fileName = filepath.substr('src/resources/templates/'.length);
                        return fileName.substr(0, fileName.length - '.html'.length);
                    }
                }
            }
        },

        compass: {
            dev: {
                options :{
                    sassDir: 'src/resources/sass',
                    cssDir: 'src/resources/css',
                    outputStyle: 'expanded',
                    relativeAssets: true,
                    imagesDir: 'src/resources/img',
                    fontsDir: 'src/resources/fonts'
                }
            }
        },

        watch: {
            index : {
                files : ['src/resources/index-template.html'],
                tasks: ['copy:index', 'copy:to-www-index', 'copy:www-to-browser-index']
            },
            css : {
                files: ['src/resources/sass/**/*.scss'],
                tasks: ['compass:dev', 'copy:to-www-css', 'copy:www-to-browser-css']
            },
            fonts : {
                files: ['src/resources/fonts/**/*.scss'],
                tasks: ['copy:to-www-fonts', 'copy:www-to-browser-fonts']
            },
            templates: {
                files: ['src/resources/templates/**/*.html'],
                tasks: ['browserify-jst', 'browserify', 'copy:to-www-js', 'copy:www-to-browser-js']
            },
            js: {
                files: ['src/js/**/*.js'],
                tasks: ['jshint', 'browserify', 'copy:to-www-js', 'copy:www-to-browser-js']
            }
        },

        replace : {
            'app-manifest' : {
                options: {
                    patterns: [
                        {
                            match: /android:icon="@drawable\/icon"/,
                            replacement : ''
                        }
                    ]
                },
                files: [
                    {
                        src: ["platforms/android/AndroidManifest.xml"],
                        dest: "platforms/android/AndroidManifest.xml"
                    }
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-browserify-jst');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-replace');

    grunt.registerTask('copy-to-www', [
        'copy:to-www-index',
        'copy:to-www-js',
        'copy:to-www-css',
        'copy:to-www-fonts',
        'copy:to-www-templates'
    ]);

    grunt.registerTask('build', [
        'clean:project',
        'clean:hammer-jquery',
        'copy:bootstrap',
        'copy:fontawesome',
        'jshint',
        'browserify-jst',
        'browserify',
        'compass:dev',
        'copy:index',
        'clean:www',
        'copy-to-www'
    ]);

    grunt.registerTask('install-plugins', [
        'exec:plugin-console'
    ]);

    grunt.registerTask('build-browser',
        'create cordova app for browser',
        [
            'clean:browser',
            'clean:generated-app',
            'clean:generated',
            'exec:create-app',
            'copy:generated',
            'clean:generated-app',
            'build',
            'exec:add-platform-browser',
            'install-plugins'
        ]
    );

    grunt.registerTask(
        'serve-browser',
        'run app in web browser',
        [
            'exec:serve-browser'
        ]
    );

    grunt.registerTask(
        'watch-browser',
        'update changed files for served web browser',
        [
            'watch'
        ]
    );

    grunt.registerTask('build-ios', [
        'clean:ios',
        'clean:generated-app',
        'clean:generated',
        'exec:create-app',
        'copy:generated',
        'clean:generated-app',
        'build',
        'exec:add-platform-ios',
        'install-plugins',
        'exec:build-ios'
    ]);

    grunt.registerTask('build-android', [
        'clean:android',
        'clean:phonegap-generated-app',
        'clean:phonegap-generated',
        'exec:create-app',
        'copy:phonegap-generated',
        'clean:phonegap-generated-app',
        'build',
        'exec:add-platform-android',
        'install-plugins',
        'replace',
        'exec:build-android'
    ]);
};
