var _ = require('underscore'),
	$ = require('jquery'),
	Backbone = require('backbone'),
	utils = require('../utils');

module.exports = Backbone.View.extend({
    className : 'home-view',
	template : utils.template('home-view'),

	initialize : function(options){
		//
	},

	render : function(){
		var $el = $(this.el);
		$el.empty();
		$el.append(this.template({}));
		this._initEventHandlers();
		return this;
	},

	_initEventHandlers : function(){
		this.$('.btn[data-command]').hammer().on('tap', function(evt){
			var $target = $(evt.target);
			var cmd = $target.data('command');
			window.alert('Command: ' + cmd);
		});
	}
});