var jst = require('./jst');
module.exports = {
    template : function(templateName){
        console.log('loading template: ' + templateName);
        return jst(templateName);
    }
};